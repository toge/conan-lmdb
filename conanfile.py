from conans import ConanFile, CMake, tools, AutoToolsBuildEnvironment
import os
import shutil

class LmdbConan(ConanFile):
    name            = "lmdb"
    version         = "0.9.24"
    license         = "OpenLDAP Public License"
    url             = "https://github.com/LMDB/lmdb/"
    description     = "Lightning Memory-Mapped Database from Symas"
    settings        = "os", "compiler", "build_type", "arch"
    options         = {"shared": [True, False]}
    default_options = "shared=False"
    generators      = "cmake"

    def source(self):
        tools.get("https://github.com/LMDB/lmdb/archive/LMDB_{}.zip".format(self.version))
        shutil.move("lmdb-LMDB_{}".format(self.version), "lmdb")

    def build(self):
        # TODO: CMakefileに対応させたい
        os.chdir("lmdb/libraries/liblmdb")
        autotools = AutoToolsBuildEnvironment(self)
        autotools.make(target="liblmdb.a")

    def package(self):
        self.copy("lmdb.h",  dst="include", src="lmdb/libraries/liblmdb", keep_path=False)
        self.copy("*.dll",   dst="bin",     keep_path=False)
        self.copy("*.so",    dst="lib",     keep_path=False)
        self.copy("*.dylib", dst="lib",     keep_path=False)
        self.copy("*.a",     dst="lib",     keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["lmdb"]
